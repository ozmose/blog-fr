---
title: Le retour du blog, en français cette fois
date: 2020-09-06
description: Après trois ans de silence, ça me démange de me remettre à partager mes divagations diverses.
category: journal
---

3 ans que je n'ai rien posté sur <https://blog.mose.com>.

Ce blog en anglais est surtout technique mais a débordé largement de cette limite pour aborder des sujets plus variés. A cette époque-là (de 2013 a 2017) je publiais une lettre d'information technique hebdomadaire en anglais nommée [Greenruby][gr]. C'était essentiellement une collection de liens de la semaine, mais j'y insérais, comme un passager clandestin, des textes et propos de type éditorial. Puis je les isolais dans le blog. Dans l'élan, j'y postais aussi des réflexions sur l'intelligence collective, l'état de l'évolution technologique, le rôle de l'humain dans cette évolution, et ce genre de choses.

Depuis ces 3 ans, je suis revenu en contact avec des groupes francophones, d'abord en montant l'infrastructure des [crapauds fous][crap], puis en zonant sur divers serveurs discord, telegram, rocketchat, et autres. Du coup j'ai perdu cette habitude et ce contexte purement anglophone.

Pendant ce temps j'ai aussi légèrement re-orienté mes pôles d'intérêt, en utilisant mes explorations et expériences communautaires pour creuser quelques théories sur l'intelligence collective. J'ai commandé des livres en papier. Et puis j'ai acquis une liseuse Kobo et collecté quelques centaines d'ouvrages numériques. Du fait que je m'intéressais déjà à l'aspect historiographique de l'intelligence collective, il me semblait nécessaire de creuser un peu sur le plan philosophique, anthropologique, ethnologique, psychologique, et autres domaines des sciences humaines.

Il semble maintenant utile et judicieux de me remettre à l'écriture, ne serais-ce que pour prendre des notes sur le cheminement de pensée que je parcours. Il est bien possible que ca soit un peu poussif, vous pardonnerez la lourdeur du style. Mais j'espère que ca va se fluidifier progressivement.

Allez, on verra bien ce que ça donne ...


[gr]: https://greenruby.mose.com
[crap]: https://crapaud-fou.org