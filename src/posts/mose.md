---
title: C'est quoi Mose? C'est vraiment ton nom?
date: "2020-09-06"
description: "test page"
category: page
---

Depuis 1992, ce nom m'accompagne comme un fidèle ami. Dans un soucis d'émancipation, d'appropriation de mon identité, j'ai crée ce nom à l'époque ou j'avais encore une activité artistique. Dans ce milieu-là, il est chose courante que d'avoir un nom d'auteur.

Quand je dessinais à la main, depuis mon plus jeune age, je signais Sem, qui est une sorte de dérivé de mes initiales. Puis j'ai fait mon premier dessin sur une tablette graphique, je me suis dit que c'était le moment de signer sous un autre nom, mose, qui s'en rapprochait. Ca sonnait bien, et c'était culturellement assez peu définissable, tout en restant dans un registre méditerranéen.

Au fil des années, cet exercice qui consiste à en faire adopter l'usage m'a satisfait. Certains le refusent ou se plaignent que mon nom ne soit pas composé d'un prénom suivi du nom de mes parents. Ceux-la manquent de la largeur d'esprit que j'espère trouver dans mon cercle relationnel. Ils m'évitent volontiers, et ça m'arrange bien.

C'est en fait bien plus tôt, étant gamin, que j'avais vécu l'expérience du nom en tant que facteur filtrant. Il se trouve que mon nom de naissance m'attirait la sympathie de gens que je ne souhaitais pas vraiment fréquenter. Des nostalgiques du temps ou la noblesse s'affirmait d'une particule, et ceux pour qui avoir un nom de saint signifie une marque de révérence pour la religion catholique.

N'étant ni royaliste ni croyant, ça m'a valu, d'évoluer avec des amis contre nature. Mais comme l'ancrage puissamment anarchiste et athéiste de mes convictions n'était pas à l'époque aussi conscient et affirmé. Je ne sentais qu'un malaise diffus. Rétrospectivement, cependant, j'en ai acquis la conviction intime que la façon dont on nomme quelque chose conditionne son contexte.

Par ailleurs j'ai acquis (ou plutôt loué) le domaine <https://mose.com> en 96. A cette époque il était encore possible d'avoir accès a des noms de domaine courts. J'y ai mis mon curriculum et j'ai soigneusement renouvelé le domaine tous les ans. 
