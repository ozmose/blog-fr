---
title: L' intelligence collective dans les communautés en ligne
date: 2021-05-08
description: Que sont les intelligences collectives - Un retour détaillé d'expérience concrète dans le cadre de la mise en place de communautés horizontales en ligne.
category: page
---

Ça fait maintenant pas loin de 20 ans que je m'intéresse à cette chose multimorphe qu'est l'intelligence collective. J'en parle autour de moi, mais je constate que bien souvent chacun a une definition qui lui est propre de cette chose-la. Il m'apparaît donc nécessaire de clarifier ce que j'entends par la.

J'ai déjà [écrit à ce sujet][1] jusqu'en 2017 mais c'est en anglais et plus orienté organisation professionnelle. Depuis cette époque j'expérimente en intelligence collective dans le contexte de communautés francophones en ligne, et ces nouvelles expériences m'ont apporté des éléments de réflexion supplémentaires.

## Principes de base

**todo**

## Définitions

Avant toute chose, gardons a l'esprit que l'intelligence collective est un phénomène que l'ont peut constater comme étant le résultat d'une collaboration collective. Ce n'est pas une méthodologie. Quand on dit, par soucis de simplicité: "*Nous sommes organisés en intelligence collective*", ce qu'on veut vraiment dire c'est "*Nous sommes organisés de façon a favoriser l'émergence d'une intelligence collective*". Mais c'est un peu long.

### Anthropologique

L'ouvrage qui pose les bases les plus signifiantes sur le plan anthropologique est celui de [Pierre Levy][5] [L'intelligence collective
Pour une anthropologie du cyberespace][4] qui des 1994 au début de l'apparition d'internet décèle un terrain propice à la possible prédominance de l'intelligence collective dans le cyberespace. Mais ensuite il y a eu la bulle internet, la marchandisation massive du net. Même si structurellement le réseau global prédispose à l'émergence d'intelligences collectives, il y a encore à ce jour une lutte active pour la faire prévaloir.

### Multiple

L'autre ouvrage de reference sur l'intelligence collective vient du MIT, qui a ouvert un [centre d'etude de l'intelligence collective][9]. L'ouvrage collectif [Handbook of Collective Intelligence][16] dirigé par Thomas Malone fait un inventaire plutôt large de toutes les formes connues d'intelligence collective. Plus récemment et en français le bouquin [Découvrir l'intelligence collective][12] d'Olivier Piazza parcours également les diverses incarnations de ce mode de fonctionnement.

### Méthodologique

En ce qui me concerne, c'est le travail de Jean-François Noubel qui m'a apporté cette approche méthodologique dont j'avais besoin dans [Intelligence Collective, la révolution invisible][10], en 2004. Ce travail a inspiré de nombreux praticiens de l'intelligence collective, comme par exemple le [collectif horizontal][11].

Mais d'autres pistes méthodologiques existent. A vrai dire il y a autant de méthodologies que d'organisation qui l'adoptent. A titre d'exemple, la [Sociocratie][14] (initialement très proche de la holacratie et plutôt orientée management) est adoptée par le collectif Extinction Rébellion, avec l'avantage qu'ils le documentent publiquement très clairement [sur leur peertube][15].

Depuis une dizaine d'années cette appropriation touche beaucoup la sphère managériale. Il arrive que les intentions soient louable mais cette approche méthodologique remet souvent mal en question le paradigme hiérarchique en entreprise. On peut citer la [holacracie expliquée en BD][13].

Dans le registre méthodologique il y a un autre acteur français incontournable, qui publie plein de petites vidéos méthodologiques, c'est [Jean-Michel Cornu][16]. Il se base sur un contexte organisationnel spécifique et sa méthodologie en tient compte, mais ca reste une bonne source d'inspiration.

... et ce ne sont que les exemples qui me viennent a l'esprit, il y en a une multitude d'autres qui chacun ont choisi un cadre méthodologique différent pour en arriver au même objectif: l'émergence d'une intelligence collective opérationnelle.

### Politique

Le travail de coopération entre humains ou animaux résultant en une intelligence collective existe depuis la nuit des temps. Mais depuis l'apparition d'internet dans la sphère publique, cette logique d'organisation a connu un regain de pertinence. Il faut dire que les logiques collectives ont fait place progressivement depuis plusieurs siècles a une logique hiérarchique ou l'intelligence est en quelque sorte le privilège de quelques uns en haut d'une pyramide.

Il y a un rapport visible entre la propriété et la division du travail et la [proletarisation][2]. De même un tel rapport existe entre la gestion des communs et l'intelligence collective. Il est bon de lire [l'entr'aide][3] de Kropotkine pour s'en souvenir. On oppose donc en general une organisation en intelligence collective à la gestion pyramidale des groupes.

Ceci étant posé il serait délicat de faire l'impasse sur l'idéologie anarchiste, qui tend à promouvoir ce type d'organisation horizontale, libéré de la verticalité pesante qui plaît tant aux dominants. Mais que je sache, il y a peu de penseurs anarchistes qui aient publié a ce sujet. A moins que de nos jours, parler d'intelligence collective, c'est au final une nouvelle façon de parler d'anarchie.

Le fait est que le modèle d'organisation en intelligence collective ne nécessite pas d'adhésion idéologique a l'anarchisme. Dans le milieu du logiciel libre ce mode de fonctionnement a ete adopté pour des raisons pragmatiques liées a l'environnement collaboratif, et nombreux des contributeurs et des promoteurs du logiciel libre hausseraient les épaules si on leur parlait d'anarchisme.

Mais il est à noter que l'intelligence collective est décisive dans les [Zones d'Autonomie Temporaires][7] de Hakim Bey en 91, proclamée dans la [Déclaration d'Indépendance du Cyberespace][6] de John Perry Barlow en 96, induite dans [la cathédrale et le bazaar][8] de Eric Raymond en 98. Elle est un des piliers de la cyberculture apparue à la naissance d'internet.

## Principes

### Les conditions nécessaires

### Les facteurs favorables

## Par l'exemple

...


[1]: https://blog.mose.com/tags/collective-intelligence/
[2]: https://arsindustrialis.org/prol%C3%A9tarisation
[3]: https://www.colibris-lemouvement.org/magazine/kropotkine-lentraide-facteur-devolution
[4]: https://www.leslibraires.fr/livre/6349419-l-intelligence-collective-pour-une-anthropolog--pierre-levy-la-decouverte
[5]: https://fr.wikipedia.org/wiki/Pierre_L%C3%A9vy_(philosophe)
[6]: http://editions-hache.com/essais/barlow/barlow2.html
[7]: https://fr.theanarchistlibrary.org/library/hakim-bey-zone-autonome-temporaire
[8]: http://www.linux-france.org/article/these/cathedrale-bazar/cathedrale-bazar_monoblock.html
[9]: https://cci.mit.edu/
[10]: https://mose.fr/intelligence-collective.pdf
[11]: https://colhor.wordpress.com/initiation-a-lintelligence-collective/
[12]: https://www.leslibraires.fr/livre/14802213-decouvrir-l-intelligence-collective-olivier-piazza-intereditions
[13]: https://labdsurlholacracy.com/bande-dessinee-holacracy#page-1
[14]: https://sociocratie.net/Theorie/index.php
[15]: https://tube.extinctionrebellion.fr/accounts/sensibilisation_formation_xrfrance/video-channels
[16]: https://scripts.mit.edu/~cci/HCI/index.php?title=Main_Page
