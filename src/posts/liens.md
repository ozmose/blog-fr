---
title: Quelques liens pour mémoire
date: "2020-09-06"
description: "Ce sont juste des liens que je souhaite garder pour référence"
category: page
---

Je garde ici quelques liens de référence pour en garder une trace ...

- [Le mythe de l’accoutumance provoquée par les drogues](https://sencanada.ca/content/sen/committee/371/ille/presentation/alexender-f.htm)
- [Les langages documentaires](https://www.cairn.info/revue-documentaliste-sciences-de-l-information-2007-1-page-18.htm#)

et en anglais

- [Big Data’s unintended side effect](https://www.socialcooling.com/)