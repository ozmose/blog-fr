Mose blog FR
================

dev env
-------------

    nvm use 14
    npm install -g gatsby-cli
    npm install
    npm start
    open http://localhost:8000

- https://www.gatsbyjs.com/docs/
- https://www.gatsbyjs.com/tutorial

Copy is Right
----------------

- js files are under BSD zero clause license
- md files are CC-by-sa-nc
