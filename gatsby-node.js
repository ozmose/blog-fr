const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const resultPosts = await graphql(
    `
      {
        allMarkdownRemark(
          filter: { frontmatter: { category: { ne: "page" }}},
          sort: { fields: frontmatter___date, order: DESC },
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
              }
            }
          }
        }
      }
    `
  )
  const blogPage = path.resolve(`./src/templates/blog-page.js`)
  const resultPages = await graphql(
    `
      {
        allMarkdownRemark(
          filter: { frontmatter: { category: { eq: "page" }}},
          sort: { fields: frontmatter___date, order: DESC },
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              id
              excerpt
              html
              frontmatter {
                title
                description
              }
            }
          }
        }
      }
    `
  )

  if (resultPosts.errors) {
    throw resultPosts.errors
  }

  // Create blog posts pages.
  const posts = resultPosts.data.allMarkdownRemark.edges

  posts.forEach((post, index) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node

    createPage({
      path: post.node.fields.slug,
      component: blogPost,
      context: {
        slug: post.node.fields.slug,
        previous,
        next,
      },
    })
  })


  // Create pages.
  const pages = resultPages.data.allMarkdownRemark.edges
  pages.forEach((page) => {
    createPage({
      path: page.node.fields.slug,
      component: blogPage,
      context: { 
        page: page.node
      },
    })
  })

}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
